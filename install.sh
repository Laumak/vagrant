sudo apt-get update

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

sudo add-apt-repository -y ppa:ondrej/php5-5.6 && sudo apt update
# Apache2 + modit
sudo apt-get install -y apache2 libapache2-mod-php5

# Mysql + git
sudo apt-get install -y mysql-server
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES;"
sudo service mysql restart

# PHP 5.6 + modit
sudo apt-get install -y php5 php5-curl php5-gd php5-mcrypt php5-readline php5-mysql

# Git, Vim, Nano, curl
sudo apt-get install -y git-core vim nano curl python-software-properties

sudo a2enmod rewrite

sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/disable_functions = .*/disable_functions = /" /etc/php5/cli/php.ini

sudo service apache2 restart

# Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer